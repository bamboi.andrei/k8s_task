Initializare:
minikube start 
kubectl get pods ## or kubectl cluster-info/kubectl | get nodes/minikube status

Deployment nginx:
kubectl apply -f nginx.yml | kubectl get services | kubectl get deployments | kubectl logs services/nginx-service | kubectl logs deployments/nginx-deployment

Job + Cronjob:
kubectl apply -f job.yml | kubectl apply -f cronjob.yaml
kubectl get jobs | kubectl get cronjobs
kubectl logs jobs/printenv-job | kubectl logs cronjobs/printenv-cronjob

Configmap output pod:
kubectl apply -f configmap.yaml | kubectl get configmap
kubectl apply -f ConfPrinterPod.yaml | kubectl logs pods/ConfPrinterPod

Secret output pod:
kubectl apply -f secret.yaml | kubectl get secrets
kubectl apply -f secret-printer-pod.yaml | kubectl logs pods/secret-printer-pod